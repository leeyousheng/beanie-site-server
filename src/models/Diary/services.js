import { getCommonServices } from '../common';
import { DefaultModel } from './Diary';

const commonServices = getCommonServices(DefaultModel)('diaries', { create: ['diary-write', 'admin'] });

const specific = {
  createDiaries: input => (user) => {
    const author = {
      id: null,
      displayname: 'anon',
      pic: null,
      ...user,
    };
    return commonServices.createDiaries({ ...input, author })(user);
  },
};

export default Object.assign({}, commonServices, specific);
