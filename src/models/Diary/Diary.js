import { actions as tagActions } from '../Tag';

const type = `
  type Diary {
    id: ID
    title: String
    entryDate: Date
    text: String
    tags: [Tag]
    author: Author
  }
  type Author {
    displayname: String
    pic: String
  }
`;

const input = `
  input DiaryInput {
    title: String!
    text: String!
    entryDate: Date!
    tags: [ID]
  }
`;

const resolvers = {
  Diary: {
    tags: (data) => {
      if (!data.tags || data.tags.length < 1) {
        return undefined;
      }
      return tagActions.getByIdsTags(...data.tags);
    },
  },
};

export const DefaultModel = {
  id: '',
  title: '',
  createdOn: '',
  text: '',
  tags: [],
};

export default {
  typeDefs: { model: `${type}${input}` },
  resolvers,
};
