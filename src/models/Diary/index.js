import { schemaHelper } from '../../utils';
import Diary from './Diary';
import services from './services';

const allModels = [Diary];

export const typeDefs = schemaHelper.combineTypeDefs(...allModels);
export const resolvers = schemaHelper.combineResolvers(...allModels);
export const actions = services;

export default {
  typeDefs,
  resolvers,
  actions,
};
