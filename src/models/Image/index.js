import { schemaHelper } from '../../utils';
import Image from './Image';

const allModels = [Image];

export const typeDefs = schemaHelper.combineTypeDefs(...allModels);
export const resolvers = schemaHelper.combineResolvers(...allModels);
export const actions = schemaHelper.combineActions(...allModels);

export default {
  typeDefs,
  resolvers,
  actions,
};
