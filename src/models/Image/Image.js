const type = `
  type Image {
    src: String
    alt: String
  }
`;

const input = `
  input ImageInput {
    src: String
    alt: String
  }
`;

const resolvers = {
  Image: {},
};

export default { typeDefs: { model: `${type}${input}` }, resolvers };
