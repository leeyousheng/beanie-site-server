const type = `
  type Tag {
    id: ID
    code: String
    text: String
    type: String
  }
`;

const input = `
  input TagInput {
    code: String!
    text: String!
    type: String!
  }
`;

const resolvers = {
  Tag: {},
};

export const DefaultModel = {
  text: '',
  type: '',
};

export default { typeDefs: { model: `${type}${input}` }, resolvers };
