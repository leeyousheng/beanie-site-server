import { schemaHelper } from '../../utils';
import Tag from './Tag';
import services from './services';

const allModels = [Tag];

export const typeDefs = schemaHelper.combineTypeDefs(...allModels);
export const resolvers = schemaHelper.combineResolvers(...allModels);
export const actions = services;

export default {
  typeDefs,
  resolvers,
  actions,
};
