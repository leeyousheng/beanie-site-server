import { getCommonServices } from '../common';
import { DefaultModel } from './Tag';

const commonServices = getCommonServices(DefaultModel)('tags', { create: ['admin'] });
const specific = {};

export default Object.assign({}, commonServices, specific);
