const type = `
  type Profile {
    id: ID
    name: String
    email: String
    pic: String
    authgroup: [String]
  }
`;

const input = `
  input ProfileInput {
    id: ID!
    name: String!
    email: String!
    pic: String
    authgroup: [String]
  }
`;

const resolvers = {};

export const DefaultModel = {
  id: '',
  name: '',
  email: '',
  pic: '',
  authgroup: [],
};

export default {
  typeDefs: { model: `${type}${input}` },
  resolvers,
};
