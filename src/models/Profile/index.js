import { schemaHelper } from '../../utils';
import Profile from './Profile';
import services from './services';

const allModels = [Profile];

export const typeDefs = schemaHelper.combineTypeDefs(...allModels);
export const resolvers = schemaHelper.combineResolvers(...allModels);
export const actions = services;

export default {
  typeDefs,
  resolvers,
  actions,
};
