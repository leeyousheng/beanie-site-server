import { getCommonServices } from '../common';
import { DefaultModel } from './Profile';
import { getUidFromToken } from '../../firebase';

const commonServices = getCommonServices(DefaultModel)('profiles', {
  getById: ['self', 'admin'],
  getByIds: ['admin'],
  getAll: ['admin'],
  create: ['admin'],
});

const getProfileFromToken = token => (
  new Promise((resolve) => {
    if (!token) {
      return resolve(undefined);
    }

    return resolve(getUidFromToken(token)
      .then((uid) => {
        console.log('here2');
        return commonServices.getByIdProfiles(uid)({ uid });
      }));
  })
);

const specific = { getProfileFromToken };

export default Object.assign({}, commonServices, specific);
