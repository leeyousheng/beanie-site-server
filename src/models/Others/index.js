import { schemaHelper } from '../../utils';
import Server from './Server';

const allModels = [Server];
export default {
  typeDefs: schemaHelper.combineTypeDefs(...allModels),
  resolvers: schemaHelper.combineResolvers(...allModels),
};
