const type = `
  type Server {
    name: String
    version: String
    type: String
  }
`;

const input = '';

const resolvers = {
  Server: {},
};

export default { typeDefs: { model: `${type}${input}` }, resolvers };
