const type = `
  type HighlightItem {
    type: String
    id: ID
}
`;

const input = `
  input HighlightItemInput {
    type: String!
    id: ID!
  }
`;

const resolvers = {
  EventDetail: {},
};

export default { typeDefs: { model: `${type}${input}` }, resolvers };
