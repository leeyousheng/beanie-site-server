const type = `
  type Highlight {
    id: ID
    primary: HighlightItem
    secondary: HighlightItem
    others: [HighlightItem]
  }
`;

const input = `
  input HighlightInput {
    primary: HighlightItemInput
    secondary: HighlightItemInput
    others: [HighlightItemInput]
  }
`;

const resolvers = {
  Highlight: {},
};

export const DefaultModel = {
  id: '',
  primary: undefined,
  secondary: undefined,
  others: [],
};

export default {
  typeDefs: { model: `${type}${input}` },
  resolvers,
};
