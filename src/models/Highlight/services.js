import { getCommonServices } from '../common';
import { DefaultModel } from './Highlight';

const commonServices = getCommonServices(DefaultModel)('highlights', { create: ['admin'] });
const specific = {};

export default Object.assign({}, commonServices, specific);
