import { schemaHelper } from '../../utils';
import Highlight from './Highlight';
import HighlightItem from './HighlightItem';
import services from './services';

const allModels = [Highlight, HighlightItem];

export const typeDefs = schemaHelper.combineTypeDefs(...allModels);
export const resolvers = schemaHelper.combineResolvers(...allModels);
export const actions = services;

export default {
  typeDefs,
  resolvers,
  actions,
};
