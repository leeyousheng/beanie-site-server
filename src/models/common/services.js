import { config } from 'firebase-functions';

import { db } from '../../firebase';

const createCollectionServices = (collection, services) => {
  const resultObj = {};
  const procCollection = collection.charAt(0).toUpperCase() + collection.slice(1);
  Object.keys(services).forEach((key) => { resultObj[`${key}${procCollection}`] = services[key]; });

  return resultObj;
};

export const getRoute = (name) => {
  const apiRoutes = config()['api-routes'];
  const route = apiRoutes[name];
  if (!route) {
    throw new Error(`Route ${name} not found.`);
  }
  return route;
};

const getServices = defaultModel => (name, authLvls = {}) => {
  const collection = getRoute(name);
  const {
    getAll,
    getById,
    getByIds,
    create,
  } = authLvls;
  return createCollectionServices(name, {
    getAll: () => user => db.getCollection(getAll)(user, collection),
    getById: id => user => db.getDoc(defaultModel)(collection)(getById)(user, id),
    getByIds: (...ids) => user => db.getDocs(defaultModel)(collection)(getByIds)(user, ...ids),
    create: input => user => db.create(defaultModel)(create)(user, { collection }, input),
  });
};

export default getServices;
