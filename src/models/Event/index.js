import { schemaHelper } from '../../utils';
import Event from './Event';
import EventDetail from './EventDetail';
import EventOption from './EventOption';
import services from './services';

const allModels = [Event, EventDetail, EventOption];

export const typeDefs = schemaHelper.combineTypeDefs(...allModels);
export const resolvers = schemaHelper.combineResolvers(...allModels);
export const actions = services;

export default { typeDefs, resolvers, actions };
