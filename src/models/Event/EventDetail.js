const type = `
  type EventDetail {
    name: String
    icon: String
    text: String
}
`;

const input = `
  input EventDetailInput {
    name: String
    icon: String
    text: String
  }
`;

const resolvers = {
  EventDetail: {},
};

export default { typeDefs: { model: `${type}${input}` }, resolvers };
