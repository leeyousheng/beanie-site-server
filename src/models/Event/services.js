import { getCommonServices } from '../common';
import { DefaultModel } from './Event';

const commonServices = getCommonServices(DefaultModel)('events', { create: ['admin'] });
const specific = {};

export default Object.assign({}, commonServices, specific);
