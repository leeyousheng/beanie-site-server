const type = `
  type EventOption {
    name: String
    text: String
    type: String
  }
`;

const input = `
  input EventOptionInput {
    name: String
    text: String
    type: String
  }
`;

const resolvers = {
  EventOption: {},
};

export default { typeDefs: { model: `${type}${input}` }, resolvers };
