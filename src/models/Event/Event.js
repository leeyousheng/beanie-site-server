const type = `
  type Event {
    id: ID
    title: String
    eventDetails: [EventDetail]
    description: String
    options: [EventOption]
    image: Image
  }
`;

const input = `
  input EventInput {
    title: String
    eventDetails: [EventDetailInput]
    description: String
    options: [EventOptionInput]
    image: ImageInput
  }
`;

const resolvers = {
  Event: {},
};

export const DefaultModel = {
  title: '',
  eventDetails: [],
  description: '',
  options: [],
  image: undefined,
};

export default { typeDefs: { model: `${type}${input}` }, resolvers };
