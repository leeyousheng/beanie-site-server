import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';

const type = `
  scalar Date
`;

const input = '';

const resolvers = {
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date scalar type',
    parseValue(value) {
      return new Date(value);
    },
    serialize(value) {
      return value.toDate();
    },
    parseLiteral(ast) {
      return ast.kind === Kind.INT ? new Date(ast.value) : null;
    },
  }),
};

export default { typeDefs: { model: `${type}${input}` }, resolvers };
