import { schemaHelper } from '../../utils';
import Date from './Date';

const allModels = [Date];

export const typeDefs = schemaHelper.combineTypeDefs(...allModels);
export const resolvers = schemaHelper.combineResolvers(...allModels);
export const actions = null;

export default { typeDefs, resolvers, actions };
