import { schemaHelper } from '../utils';
import Event from './Event';
import Image from './Image';
import Diary from './Diary';
import Tag from './Tag';
import Highlight from './Highlight';
import Others from './Others';
import Scalar from './Scalar';
import Profile from './Profile';

const allModels = [Event, Image, Others, Diary, Tag, Highlight, Scalar, Profile];
export default {
  typeDefs: schemaHelper.combineTypeDefs(...allModels),
  resolvers: schemaHelper.combineResolvers(...allModels),
};
