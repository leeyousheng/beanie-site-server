import { ApolloServer } from 'apollo-server-cloud-functions';
import { https } from 'firebase-functions';

import { createServer } from './createServer';

export const api = https.onRequest((req, res) => (
  createServer(ApolloServer).createHandler()(req, res)
));
