// Project imports
import { schemaHelper } from './utils';
import models from './models';
import controllers from './controllers';

const allSchemas = [models, controllers];
const resolvers = schemaHelper.createRootResolver(schemaHelper.combineResolvers(...allSchemas));
const typeDefs = schemaHelper.createRootTypeDefs(schemaHelper.combineTypeDefs(...allSchemas));

export const createServer = Server => (
  new Server({
    typeDefs,
    resolvers,
    context: ({ req }) => ({ authorization: req.headers.authorization }),
  })
);

export default {
  createServer,
};
