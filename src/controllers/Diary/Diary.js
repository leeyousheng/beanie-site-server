import { actions } from '../../models/Diary';
import { basicCtrl } from '../common';

const typeDefs = {
  query: `
    allDiaries: [Diary]
    diary(id: ID!): Diary
  `,
  mutation: `
    createDiary(input: DiaryInput!): Diary
  `,
};

const resolvers = {
  query: {
    allDiaries: basicCtrl(() => actions.getAllDiaries()),
    diary: basicCtrl(({ id }) => actions.getByIdDiaries(id)),
  },
  mutation: {
    createDiary: basicCtrl(({ input }) => actions.createDiaries(input)),
  },
};

export default { typeDefs, resolvers };
