import { schemaHelper } from '../../utils';
import Diary from './Diary';

const allModels = [Diary];
export default {
  typeDefs: schemaHelper.combineTypeDefs(...allModels),
  resolvers: schemaHelper.combineResolvers(...allModels),
};
