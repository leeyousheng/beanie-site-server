import { schemaHelper } from '../../utils';
import Profile from './Profile';

const allModels = [Profile];
export default {
  typeDefs: schemaHelper.combineTypeDefs(...allModels),
  resolvers: schemaHelper.combineResolvers(...allModels),
};
