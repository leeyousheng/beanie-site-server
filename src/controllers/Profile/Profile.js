import { actions } from '../../models/Profile';
import { basicCtrl } from '../common';

const typeDefs = {
  query: `
    allProfiles: [Profile]
    profile(id: ID!): Profile
  `,
  mutation: `
    createProfile(input: ProfileInput!): Profile
  `,
};

const resolvers = {
  query: {
    allProfiles: basicCtrl(() => actions.getAllProfiles()),
    profile: basicCtrl(({ id }) => actions.getByIdProfiles(id)),
  },
  mutation: {
    createProfile: basicCtrl(({ input }) => actions.createProfiles(input)),
  },
};

export default { typeDefs, resolvers };
