import { schemaHelper } from '../utils';
import Event from './Event';
import Others from './Others';
import Diary from './Diary';
import Tag from './Tag';
import Highlight from './Highlight';
import Profile from './Profile';

const allModels = [Event, Others, Diary, Tag, Highlight, Profile];
export default {
  typeDefs: schemaHelper.combineTypeDefs(...allModels),
  resolvers: schemaHelper.combineResolvers(...allModels),
};
