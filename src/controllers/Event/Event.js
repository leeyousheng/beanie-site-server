import { actions } from '../../models/Event';
import { basicCtrl } from '../common';

const typeDefs = {
  query: `
    allEvents: [Event]
    event(id: ID!): Event
  `,
  mutation: `
    createEvent(input: EventInput!): Event
  `,
};

const resolvers = {
  query: {
    allEvents: basicCtrl(() => actions.getAllEvents()),
    event: basicCtrl(({ id }) => actions.getByIdEvents(id)),
  },
  mutation: {
    createEvent: basicCtrl(({ input }) => actions.createEvents(input)),
  },
};

export default { typeDefs, resolvers };
