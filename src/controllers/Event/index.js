import { schemaHelper } from '../../utils';
import Event from './Event';

const allModels = [Event];
export default {
  typeDefs: schemaHelper.combineTypeDefs(...allModels),
  resolvers: schemaHelper.combineResolvers(...allModels),
};
