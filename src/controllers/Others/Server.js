import { db } from '../../firebase';

const typeDefs = {
  query: `
    server: Server
  `,
};

const getServerInfo = () => db.collection('config').doc('api').get();

const resolvers = {
  query: {
    server: () => getServerInfo(),
  },
};

export default { typeDefs, resolvers };
