import { actions } from '../../models/Profile';

export const basicCtrl = func => (...args) => (
  actions.getProfileFromToken(args[2].authorization)
    .then(user => func(args[1])(user))
);
