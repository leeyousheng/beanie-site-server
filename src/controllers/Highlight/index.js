import { schemaHelper } from '../../utils';
import Highlight from './Highlight';

const allModels = [Highlight];
export default {
  typeDefs: schemaHelper.combineTypeDefs(...allModels),
  resolvers: schemaHelper.combineResolvers(...allModels),
};
