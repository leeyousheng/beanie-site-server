import { actions } from '../../models/Highlight';
import { basicCtrl } from '../common';

const typeDefs = {
  query: `
    allHighlights: [Highlight]
    highlight(id: ID!): Highlight
  `,
  mutation: `
    createHighlight(input: HighlightInput!): Highlight
  `,
};

const resolvers = {
  query: {
    allHighlights: basicCtrl(() => actions.getAllHighlights()),
    highlight: basicCtrl(({ id }) => actions.getByIdHighlights(id)),
  },
  mutation: {
    createHighlight: basicCtrl(({ input }) => actions.createHighlights(input)),
  },
};

export default { typeDefs, resolvers };
