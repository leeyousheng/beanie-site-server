import { schemaHelper } from '../../utils';
import Tag from './Tag';

const allModels = [Tag];
export default {
  typeDefs: schemaHelper.combineTypeDefs(...allModels),
  resolvers: schemaHelper.combineResolvers(...allModels),
};
