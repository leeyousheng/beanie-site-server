import { actions } from '../../models/Tag';
import { basicCtrl } from '../common';

const typeDefs = {
  query: `
    allTags: [Tag]
    tag(id: ID!): Tag
  `,
  mutation: `
    createTag(input: TagInput!): Tag
  `,
};

const resolvers = {
  query: {
    allTags: basicCtrl(() => actions.getAllTags()),
    tag: basicCtrl(({ id }) => actions.getByIdTags(id)),
  },
  mutation: {
    createTag: basicCtrl(({ input }) => actions.createTags(input)),
  },
};

export default { typeDefs, resolvers };
