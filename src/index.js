import { ApolloServer } from 'apollo-server';

import { createServer } from './createServer';

createServer(ApolloServer).listen().then(({ url }) => {
  // eslint-disable-next-line no-console
  console.log(`🚀  Server ready at ${url}`);
});
