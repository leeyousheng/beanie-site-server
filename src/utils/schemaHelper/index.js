import _combineTypeDefs from './combineTypeDefs';
import _combineResolvers from './combineResolvers';
import _combineActions from './combineActions';
import _createRootResolver from './createRootResolver';
import _createRootTypeDefs from './createRootTypeDefs';

export const combineTypeDefs = _combineTypeDefs;
export const combineResolvers = _combineResolvers;
export const combineActions = _combineActions;
export const createRootResolver = _createRootResolver;
export const createRootTypeDefs = _createRootTypeDefs;

export default {
  combineTypeDefs,
  combineResolvers,
  combineActions,
  createRootResolver,
  createRootTypeDefs,
};
