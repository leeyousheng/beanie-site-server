import { gql } from 'apollo-server';

const generateType = (type, value) => {
  if (value && value !== '') {
    return `
      type ${type} {
        ${value}
      }
    `;
  }
  return '';
};

const createRootTypeDefs = ({
  query,
  mutation,
  subscription,
  model,
}) => (gql`
    ${generateType('Query', query)}
    ${generateType('Mutation', mutation)}
    ${generateType('Subscription', subscription)}
    ${model}
  `);

export default createRootTypeDefs;
