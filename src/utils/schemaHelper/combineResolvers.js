export default (...models) => {
  let allResolvers = {};
  models.forEach((model) => {
    const {
      query,
      mutation,
      subscription,
      ...others
    } = model.resolvers;
    allResolvers.query = { ...allResolvers.query, ...query };
    allResolvers.mutation = { ...allResolvers.mutation, ...mutation };
    allResolvers.subscription = { ...allResolvers.subscription, ...subscription };
    allResolvers = Object.assign({}, others, allResolvers);
  });
  return allResolvers;
};
