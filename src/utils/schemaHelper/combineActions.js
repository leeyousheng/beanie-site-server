export default (...models) => {
  const allActions = {};
  models.forEach(model => Object.assign(allActions, model.actions));
  return allActions;
};
