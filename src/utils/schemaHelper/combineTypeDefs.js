export default (...objs) => {
  const allTypeDefs = {};
  objs.forEach((obj) => {
    const {
      query,
      mutation,
      subscription,
      model,
    } = obj.typeDefs;
    allTypeDefs.query = `${allTypeDefs.query || ''}${query || ''}`;
    allTypeDefs.mutation = `${allTypeDefs.mutation || ''}${mutation || ''}`;
    allTypeDefs.subscription = `${allTypeDefs.subscription || ''}${subscription || ''}`;
    allTypeDefs.model = `${allTypeDefs.model || ''}${model || ''}`;
  });
  return allTypeDefs;
};
