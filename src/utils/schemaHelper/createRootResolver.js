const createRootResolver = ({
  query,
  mutation,
  subscription,
  ...others
}) => ({
  Query: { ...query },
  Mutation: { ...mutation },
  ...others,
});

export default createRootResolver;
