import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

export const getSettings = ({ firestore, firebase }) => {
  const { type, credential, storageBucket } = firestore;

  switch (type) {
    case 'local':
      if (credential === undefined) {
        throw new Error('Credential not provided');
      }
      return {
        credential: admin.credential.cert(credential),
        storageBucket,
      };
    case 'firebase':
      return firebase;
    case 'google-cloud-platform':
      return admin.credential.applicationDefault();
    default:
      throw new Error(`Unknown type ${type}`);
  }
};

const initFirebase = () => {
  if (admin.apps.length > 0) {
    // Already Initialised
    return admin.apps[0];
  }

  const settings = getSettings(functions.config());
  return admin.initializeApp(settings);
};

export default initFirebase();
