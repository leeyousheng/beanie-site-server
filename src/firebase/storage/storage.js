import firebaseAdmin from '../firebase';

export const initStorage = () => firebaseAdmin.storage().bucket();

export default initStorage();
