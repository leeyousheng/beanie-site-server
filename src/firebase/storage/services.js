import storage from './storage';

const getImageSrc = (path) => {
  if (!path) {
    return undefined;
  }

  return (storage.file(path).get()
    .then((res) => {
      const { name } = res[0];
      const { bucket, metadata } = res[1];
      return `https://firebasestorage.googleapis.com/v0/b/${bucket}/o/${encodeURIComponent(name)}?alt=media&token=${metadata.firebaseStorageDownloadTokens}`;
    })
  );
};

export default { getImageSrc };
