import { AuthenticationError } from 'apollo-server';

// Project imports
import firestore from './firestore';
import { verifyAccess } from '../auth';

const injectIdToDoc = (docId, doc) => {
  const data = doc.data();
  const obj = { id: docId };

  if (!data) {
    return Object.assign({}, obj, { error: 'Error 404: File not found' });
  }
  return Object.assign({}, obj, { ...doc.data() });
};

const injectAuditInfo = (value, user, type) => {
  const { id, displayname, pic } = Object.assign({}, { id: null, displayname: 'anon', pic: null }, user);
  const auditUser = { id, displayname, pic };
  const auditInfo = () => {
    switch (type) {
      case 'create':
        return ({
          createdOn: new Date(),
          createdBy: auditUser,
        });
      case 'update':
        return ({
          updatedOn: new Date(),
          updatedBy: auditUser,
        });
      case 'delete':
        return ({
          deletedOn: new Date(),
          deletedBy: auditUser,
        });
      default:
        throw new Error(`Invalid audit info type ${type}`);
    }
  };

  return Object.assign({}, auditInfo(), value);
};

export const create = defaultModel => (authLvls = []) => (user, { collection, id }, value) => (
  verifyAccess(authLvls, user)
    .then((success) => {
      if (!success) {
        throw new AuthenticationError('Insufficient Rights');
      }

      const auditValue = injectAuditInfo(value, user, 'create');
      const action = id
        ? firestore.collection(collection).doc(id).set(auditValue)
        : firestore.collection(collection).add(auditValue);

      return new Promise((resolve, reject) => (
        action.then(res => res.get())
          .then(res => resolve(Object.assign({}, defaultModel, injectIdToDoc(res.id, res))))
          .catch(err => reject(err))
      ));
    })
);

export const getDoc = defaultModel => collection => (authLvls = []) => (user, docId) => (
  verifyAccess(authLvls, user)
    .then((success) => {
      if (!success) {
        throw new AuthenticationError('Insufficient Rights');
      }

      return (
        firestore.collection(collection).doc(docId).get()
          .then(res => Object.assign({}, defaultModel, injectIdToDoc(docId, res)))
      );
    })
);

export const getDocs = defaultModel => collection => (authLvls = []) => (user, ...docIds) => (
  verifyAccess(authLvls, user)
    .then((success) => {
      if (!success) {
        throw new AuthenticationError('Insufficient Rights');
      }
      return Promise.all(docIds.map(docId => getDoc(defaultModel)(collection)(docId)));
    })
);

export const getCollection = (authLvls = []) => (user, collection) => (
  verifyAccess(authLvls, user)
    .then((success) => {
      if (!success) {
        throw new AuthenticationError('Insufficient Rights');
      }

      return new Promise((resolve, reject) => {
        if (!collection) {
          return reject(new Error('Invalid Collection'));
        }

        return firestore.collection(collection)
          .orderBy('createdOn', 'desc')
          .get()
          .then((res) => {
            const docs = [];
            res.forEach(doc => docs.push(injectIdToDoc(doc.id, doc)));
            return resolve(docs);
          })
          .catch(err => reject(err));
      });
    })
);

export default {
  create,
  getDoc,
  getDocs,
  getCollection,
};
