import firebaseAdmin from '../firebase';

export const initDB = () => {
  const firestore = firebaseAdmin.firestore();
  firestore.settings({ timestampsInSnapshots: true });
  return firestore;
};

export default initDB();
