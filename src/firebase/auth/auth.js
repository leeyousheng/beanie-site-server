import firebaseAdmin from '../firebase';

export const initAuth = () => {
  const auth = firebaseAdmin.auth();
  return auth;
};

export default initAuth();
