import { AuthenticationError } from 'apollo-server';

// Project imports
import auth from './auth';

export default auth;

export const getUidFromToken = (token) => {
  if (!token) {
    throw new AuthenticationError('Not authenticated');
  }

  return auth.verifyIdToken(token)
    .then(res => res.uid)
    .catch((err) => {
      throw new AuthenticationError(err.message);
    });
};

export const verifyAccess = (authLvls, user, docId) => (
  new Promise((resolve, reject) => {
    if (authLvls.length === 0) return resolve(true);
    if (!user) reject(new AuthenticationError('Not authenticated'));
    const { authgroup } = user;
    return resolve((authgroup && Array.filter(authLvls, lvl => authgroup.includes(lvl)).length > 0)
      || (authLvls.includes('self') && user.id === docId));
  })
);
